<?php
    include "db.php";
    include "query.php";
?>
<html>
    <head>
        <title>Ex4</title>
    </head>
    <body>
        <p>
        <?php
        //database connection
        $db = new DB('localhost', 'intro', 'root', '');
        $dbc = $db->connect();
        $query = new Query($dbc);
        $q = "SELECT B.title, B.author, U.name
              FROM books B JOIN users U ON B.user_id = U.id";
        $result = $query->query($q);
        echo '<br>';
        //echo $result->num_rows;
        if($result->num_rows > 0 ){
            echo '<table>';
            echo '<tr><th>Title</th><th>Author</th><th>Name_User</th></tr>';
            while($row = $result->fetch_assoc()){
                echo '<tr>';
                echo '<td>'.$row['title'].'</td><td>'.$row['author'].'</td><td>'.$row['name'].'</>';
                echo '</tr>';
            }
            echo '</table>';
        } else {
            echo "Sorry no results";
        }

        ?>
        </p>
    </body>
</html>